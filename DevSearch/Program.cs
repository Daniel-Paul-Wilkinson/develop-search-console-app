﻿using ConsoleTableExt;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Management;
using System.Threading;
using DevSearch.Enums;
using DevSearch.Const;
using DevSearch.Interfaces;
using DevSearch.Service;
using Microsoft.Extensions.DependencyInjection;

namespace DevSearch
{
    public class Program
    {


        private static void Main()
        {

            var collection = new ServiceCollection();

            collection.AddSingleton<IMenuService, MenuService>();
            collection.AddSingleton<IValidationService, ValidationService>();
            collection.AddSingleton<IFileService, FileService>();
            collection.AddSingleton<IDirectoryService, DirectoryService>();
            collection.AddSingleton<IThreadService, ThreadService>();
            collection.AddSingleton<IProgramService, ProgramService>();
            collection.AddSingleton<ITableService, TableService>();

            var serviceProvider = collection.BuildServiceProvider();

            var _fileService = serviceProvider.GetService<IFileService>();
            var _validationService = serviceProvider.GetService<IThreadService>();
            var _directoryService = serviceProvider.GetService<IDirectoryService>();
            var _threadService = serviceProvider.GetService<IThreadService>();

            var _menuService = serviceProvider.GetService<IMenuService>();

            _menuService.ShowMenu();

        }
    }
}