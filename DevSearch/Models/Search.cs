﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevSearch.Models
{
    public class Search
    {
        #region Variables
        public string AllSearchedFileCount { get; set; }
        public string AllSearchedDirectoryCount { get; set; }
        public string DirectoryCount { get; set; }
        public string FileCount { get; set; }
        public string TotalSearchedSize { get; set; }
        public string SearchTime { get; set; }
        public string Size { get; set; }
        public List<DirectoryFolder> DirectoryFolders { get; set; }
        #endregion

        #region Methods
        public static void DisplaySearchInfo(Search newSearch, Stopwatch watch, bool showFileSearchSize, string name )
        {
            Console.Clear();
            Console.WriteLine("");
            Console.WriteLine("     -----------------------------------------------------------");
            Console.WriteLine("         Search Info:");
            Console.WriteLine("             Search Time - " + watch.ElapsedMilliseconds / 1000 + "s");
            Console.WriteLine("             Searched:" + Environment.NewLine +
                              "               Directories = " + newSearch.AllSearchedDirectoryCount + Environment.NewLine +
                              "               Files = " + newSearch.AllSearchedFileCount);
            Console.WriteLine("             " + name + ":");
            Console.WriteLine("               Directory Count: " + newSearch.DirectoryCount);
            Console.WriteLine("               File Count: " + newSearch.FileCount);
            if (showFileSearchSize)
            {
                Console.WriteLine("             Total Size MB: " + newSearch.Size);
            }
            Console.WriteLine("     -----------------------------------------------------------");

        }
        #endregion
    }
}
