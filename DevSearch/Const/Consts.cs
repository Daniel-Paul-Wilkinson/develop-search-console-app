﻿namespace DevSearch.Const
{
    public static class Consts
    {
        public const string C_DRIVE = "C:";
        public const string SLASH = "/";
        public const string BACK_SLASH = "\\";
        public const string BASE_PATH = @"C:" + BACK_SLASH;
        public const string DIRECTORY_OVERVIEW = "Directory Overivew";

    }
}
