﻿using System.Collections.Generic;
using System.IO;
using DevSearch.Models;

namespace DevSearch.Interfaces
{
    public interface IFileService
    {
        void SearchFileForString(List<DirectoryFolder> list, string searchString,
            SearchOption SearchOptions = SearchOption.AllDirectories);

        void SearchFile(string path, string searchString, int resultOption);

        string FileCount(List<DirectoryFolder> folders);
    }
}