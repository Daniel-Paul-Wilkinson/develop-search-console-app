﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevSearch.Models;

namespace DevSearch.Interfaces
{
    public interface ITableService
    {
        DataTable GetColumnsIfDataExists(List<DirectoryFolder> folders);
        void GenerateTable(DataTable dataTable);
    }
}
