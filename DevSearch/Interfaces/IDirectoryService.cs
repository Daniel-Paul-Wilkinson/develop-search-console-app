﻿using System.Collections.Generic;
using System.IO;
using DevSearch.Models;

namespace DevSearch.Service
{
    public interface IDirectoryService
    {
        void GlobalSearchForDirectory();
        void MapDirectory();
        void SearchExplicitPathForDirectory();
        void GlobalSearch(List<DirectoryFolder> folders, string path);
        string DirectoryCount(string path, bool includeSubDirectories);
        long GetDirectorySize(DirectoryInfo dInfo, bool includeSubDir);
        int DirectoryCountInSearch(DirectoryInfo di, bool searchOnlyTopDirectory);
    }
}