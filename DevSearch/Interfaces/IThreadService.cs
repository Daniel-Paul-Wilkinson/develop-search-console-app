﻿using System.Collections.Generic;

namespace DevSearch.Interfaces
{
    public interface IThreadService
    {
        List<List<T>> Split<T>(List<T> collection, int size);

        void MultiThreadedSearch();
    }
}