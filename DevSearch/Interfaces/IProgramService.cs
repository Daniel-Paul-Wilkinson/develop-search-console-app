﻿using System.Collections.Generic;
using DevSearch.Enums;
using DevSearch.Models;

namespace DevSearch.Interfaces
{
    public interface IProgramService
    {
        void OpenInTextEditor(string path, EditIn editIn = EditIn.Notepad);
        long ReturnTotalMemoryConsumption(List<DirectoryFolder> myFolderList);
        void ReadPaths(List<DirectoryFolder> list);

        string FormatDirectorySize(string type, int sizes);
    }
}