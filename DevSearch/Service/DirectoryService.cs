﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevSearch.Const;
using DevSearch.Interfaces;
using DevSearch.Models;

namespace DevSearch.Service
{
    public class DirectoryService : IDirectoryService
    {
        private readonly IValidationService _validationService;
        private readonly IFileService _fileService;
        private readonly IProgramService _programService;
        private readonly ITableService _tableService;

        public DirectoryService(IValidationService validationService, IFileService fileService
            , IProgramService programService, ITableService tableService)
        {
            _validationService = validationService;
            _fileService = fileService;
            _programService = programService;
            _tableService = tableService;
        }

        public void GlobalSearchForDirectory()
        {
            Console.Clear();
            Console.WriteLine("Enter the target directory: e.g 'Images'");
            var name = Console.ReadLine();


            while (!_validationService.IsValidSearch(name))
            {
                name = Console.ReadLine();
            }

            if (string.IsNullOrEmpty(name))
            {
                name = "C:\\";
            }


            var watch = Stopwatch.StartNew();


            //for loop through all directories and sub directories
            var mainDirectory = new DirectoryInfo($@"C:\\{name}");
            var subDirectories = mainDirectory.GetDirectories("*", SearchOption.AllDirectories);

            List<Search> lists = new List<Search>();
            List<DirectoryFolder> dirList = new List<DirectoryFolder>();

            foreach (var dir in subDirectories)
            {
                dirList.Add(new DirectoryFolder(dir.FullName, dir.Name, dir.CreationTime.ToString(), null,
                    dir.Parent.ToString()));
            }


            var fullDriveSearch = new Search
            {
                DirectoryFolders = dirList,
                TotalSearchedSize = Convert.ToString(_programService.ReturnTotalMemoryConsumption(dirList) / 1000),

                //Search Included
                AllSearchedDirectoryCount = dirList.Count().ToString(),
                AllSearchedFileCount = _fileService.FileCount(dirList),
                //Top Level Overview
                DirectoryCount = Convert.ToString(new DirectoryInfo($@"C:\\{name}").EnumerateDirectories().Count()),
                FileCount = Convert.ToString(new DirectoryInfo($@"C:\\{name}").EnumerateFiles().Count()),
                Size = _programService.FormatDirectorySize("mbyte", (Convert.ToInt32(Convert.ToString(_programService.ReturnTotalMemoryConsumption(dirList) / 1000)) / 1000)),
            };

            lists.Add(fullDriveSearch);

            var d = dirList.Where(x => x.Name == name).ToList();

            watch.Stop();
            Search.DisplaySearchInfo(fullDriveSearch, watch, false, "Provided Directory Name Info");

            Console.WriteLine("");
            Console.WriteLine("You searched: " + name);
            Console.WriteLine("We found it at the following location(s):");
            foreach (var s in d)
            {
                Console.WriteLine(s.Path);
            }

            watch.Reset();

            Console.WriteLine("Click any key to continue");
            Console.ReadKey();
        }

        public void MapDirectory()
        {
            //clear console for new data.
            Console.Clear();

            //get user data
            Console.WriteLine("Please enter a directory path to search: e.g \"C:\\Development\"");
            var path = Console.ReadLine();
            while (!_validationService.IsValidSearch(path))
            {
                path = Console.ReadLine();
            }

            Console.WriteLine("Please enter a search pattern: e.g *");
            var searchPattern = Console.ReadLine();
            while (!_validationService.IsValidSearch(searchPattern))
            {
                searchPattern = Console.ReadLine();
            }

            Console.WriteLine("Do you want to search sub directories: Y/N");
            var searchOption = Console.ReadLine();
            while (!_validationService.IsValidSearch(searchOption))
            {
                searchOption = Console.ReadLine();
            }

            SearchOption searchOptions;
            if (searchOption == 'Y'.ToString())
            {
                searchOptions = SearchOption.AllDirectories;
            }
            else
            {
                searchOptions = SearchOption.TopDirectoryOnly;
            }


            //loop through all directories and sub directories
            var mainDirectory = new DirectoryInfo(path);
            var subDirectories = mainDirectory.GetDirectories(searchPattern, searchOptions);

            //create new direcotry folder list
            List<DirectoryFolder> lists = new List<DirectoryFolder>();

            //add all sub directories to list.
            foreach (var s in subDirectories)
            {
                //no need to calculate size here -->
                lists.Add(new DirectoryFolder(s.FullName, s.Name, s.CreationTime.ToString(), null,
                    s.Parent.ToString()));
            }

            //foreach over the key parent and list out all child directories.
            foreach (var thing in lists.GroupBy(x => x.ParentDirectory))
            {
                Console.WriteLine("     " + thing.Key);
                foreach (var i in thing)
                {
                    Console.WriteLine("         " + i.Name);

                }

            }

            Console.WriteLine("Press any key to continue");
            Console.ReadKey();

        }

        public void SearchExplicitPathForDirectory()
        {
            Console.Clear();
            Console.WriteLine("Please enter a directory path to search: e.g \"C:\\Development\"");
            var path = Console.ReadLine();
            while (!_validationService.IsValidSearch(path))
            {
                path = Console.ReadLine();
            }

            Console.WriteLine("Please enter a search pattern: e.g *");
            var searchPattern = Console.ReadLine();
            while (!_validationService.IsValidSearch(searchPattern))
            {
                searchPattern = Console.ReadLine();
            }

            Console.WriteLine("Do you want to search sub directories: Y/N");
            var searchOption = Console.ReadLine();
            while (!_validationService.IsValidSearch(searchOption))
            {
                searchOption = Console.ReadLine();
            }

            SearchOption searchOptions;
            if (searchOption == 'Y'.ToString())
            {
                searchOptions = SearchOption.AllDirectories;
            }
            else
            {
                searchOptions = SearchOption.TopDirectoryOnly;
            }

            try
            {
                //get the directory of all folders within the path
                List<DirectoryFolder> folders =
                    GetDirectoriesWithin(path, searchPattern, true, searchOptions);

                //use this to create the table result
                GlobalSearch(folders, path);

                Console.WriteLine();
                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.Clear();
                Console.WriteLine("Oops! no such directory exists, click any key to continue.");
                Console.WriteLine(ex.StackTrace);
                Console.ReadKey();
            }
        }

        public string DirectoryCount(string path, bool includeSubDirectories)
        {
            return Convert.ToString(DirectoryCountInSearch(new DirectoryInfo(path), true));
        }

        public void GlobalSearch(List<DirectoryFolder> folders, string path)
        {
            //created new search list which contains search info & folder list (incase we need to include multiple searches for some reason.)
            List<Search> searchList = new List<Search>();

            //count how long search has taken.
            var watch = Stopwatch.StartNew();

            //search object
            Search newSearch = null;

            //if we have folders then create a new 'populated search object'
            if (folders != null)
            {
                //Create new seach object
                newSearch = new Search
                {
                    //all folders
                    DirectoryFolders = folders,

                    //total size of all folders searched 'MB'
                    TotalSearchedSize = Convert.ToString(_programService.ReturnTotalMemoryConsumption(folders) / 1000),

                    //Search Included the following
                    AllSearchedDirectoryCount = folders.Count().ToString(),
                    AllSearchedFileCount = _fileService.FileCount(folders),

                    //Top Level Overview of the path.
                    DirectoryCount = Convert.ToString(new DirectoryInfo(path).EnumerateDirectories().Count()),
                    FileCount = Convert.ToString(new DirectoryInfo(path).EnumerateFiles().Count()),
                };
            }
            else
            {
                //empty search object if no folders
                newSearch = new Search
                {

                };
            }

            //add new search to stack
            searchList.Add(newSearch);


            // data structure for table
            DataTable d = _tableService.GetColumnsIfDataExists(folders);

            //add data to table
            if (folders != null && folders.Count() > 0)
            {
                foreach (Search search in searchList)
                {

                    foreach (var myfolder in search.DirectoryFolders)
                    {

                        d.Rows.Add(myfolder.Name, myfolder.Path,
                            _programService.FormatDirectorySize("mbyte", Convert.ToInt32(myfolder.Size)),
                            myfolder.CreatedAt);  
                    }
                }
            }
            else
            {
                d.Rows.Add("No Directories Exist Here: " + path);
            }

            //stop the stopwatch           
            watch.Stop();

            //output search details
            Search.DisplaySearchInfo(newSearch, watch, false, Consts.DIRECTORY_OVERVIEW);

            //ouput table of directories
            _tableService.GenerateTable(d);

            //reset the stop watch
            watch.Reset();

            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();

        }

        public DirectoryFolder LookUpDirectoryFolder(DirectoryFolder f, string path)
        {
            DirectoryInfo info = new DirectoryInfo(path);
            f.Size = Convert.ToString(GetDirectorySize(new DirectoryInfo(path), true));
            f.Name = info.Name;
            f.Path = info.FullName;
            f.CreatedAt = info.CreationTime.ToString();
            f.ParentDirectory = info.Parent.ToString();
            return f;
        }

        public long GetDirectorySize(DirectoryInfo dInfo, bool includeSubDir)
        {
            try
            {
                long totalSize = dInfo.EnumerateFiles()
                    .Sum(file => file.Length);

                Console.WriteLine(dInfo.FullName);

                if (includeSubDir)
                {
                    totalSize += dInfo.EnumerateDirectories()
                        .Sum(dir => GetDirectorySize(dir, true));
                }

                return totalSize;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                return 0;
            }
        }

        public int DirectoryCountInSearch(DirectoryInfo di, bool searchOnlyTopDirectory)
        {
            //used to get a count of all files in a directory or all sub directories
            SearchOption searchOption = new SearchOption();
            if (searchOnlyTopDirectory) { searchOption = SearchOption.TopDirectoryOnly; }
            else { searchOption = SearchOption.AllDirectories; }
            return Directory.GetDirectories(di.FullName, "*", searchOption).Length;
        }

        public List<DirectoryFolder> GetDirectoriesWithin(string searchPath, string searchPattern, bool includeRootInSearch, SearchOption searchOption = SearchOption.AllDirectories)
        {
            List<DirectoryFolder> directoryFolders = new List<DirectoryFolder>(); ;
            try
            {
                //get folders from a custom path, search pattern and option
                List<DirectoryInfo> folders = new DirectoryInfo(searchPath).EnumerateDirectories(searchPattern, searchOption).ToList();

                //do we want to include the root directory path in the search?
                if (includeRootInSearch)
                {
                    folders.Add(new DirectoryInfo(searchPath));
                }

                //assign all folder values and populate the list
                foreach (var folder in folders)
                {
                    directoryFolders.Add(new DirectoryFolder(
                        folder.FullName,
                        folder.Name,
                        folder.CreationTime.ToString(),
                        GetDirectorySize(folder, true).ToString(),
                        folder.Parent.ToString()
                    ));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                directoryFolders = null;
            }

            //return all folders
            return directoryFolders;
        }
    }
}
