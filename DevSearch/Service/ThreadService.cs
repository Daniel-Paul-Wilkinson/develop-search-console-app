﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DevSearch.Interfaces;
using DevSearch.Models;

namespace DevSearch.Service
{
    public class ThreadService : IThreadService
    {
        private readonly IFileService _fileService;

        public ThreadService(IFileService fileService)
        {
            _fileService = fileService;
        }

        public List<List<T>> Split<T>(List<T> collection, int size)
        {
            var chunks = new List<List<T>>();
            var chunkCount = collection.Count() / size;

            if (collection.Count % size > 0)
                chunkCount++;

            for (var i = 0; i < chunkCount; i++)
                chunks.Add(collection.Skip(i * size).Take(size).ToList());

            return chunks;
        }

        public void MultiThreadedSearch()
        {

            /*
             * https://stackoverflow.com/questions/1542213/how-to-find-the-number-of-cpu-cores-via-net-c
            */

            //variables
            var coreCount = 0;
            var physicalProcessors = 0;
            var logicLProcessors = Environment.ProcessorCount;
            var ThreadsPerCore = 2;
            SearchOption searchOptions;

            //get count of physical processors
            foreach (var item in new ManagementObjectSearcher("Select * from Win32_ComputerSystem").Get())
            {
                physicalProcessors++;
            }

            //get total number of cores on a users machine.
            foreach (var item in new ManagementObjectSearcher("Select * from Win32_Processor").Get())
            {
                coreCount += int.Parse(item["NumberOfCores"].ToString());
            }

            //ask for path / use all hardware in search premission (Pointless ATM)
            Console.Clear();
            Console.WriteLine($"Your machine has: Cores-{coreCount} PProcessors-{physicalProcessors} LProcessors-{logicLProcessors}");
            Console.WriteLine($"This means you can search using {coreCount * ThreadsPerCore} threads!");
            Console.WriteLine("Utilise all hardware for search Y/N");
            string useAll = Console.ReadLine();
            Console.WriteLine("Please enter a path");
            string path = Console.ReadLine();
            Console.WriteLine("what are we searching for?");
            string searchString = Console.ReadLine();



            //1 get all directory names in paths
            var mainDirectory = new DirectoryInfo(path);

            //find out if we want to search all directories
            searchOptions = useAll == "Y" ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;

            //get sub directories
            var subDirectories = mainDirectory.GetDirectories("*", searchOptions);

            //list
            List<DirectoryFolder> dirList = new List<DirectoryFolder>();

            //add all found directories to a list
            foreach (var dir in subDirectories)
            {
                dirList.Add(new DirectoryFolder(dir.FullName, dir.Name, dir.CreationTime.ToString(), null, dir.Parent.ToString()));
            }

            //get count of folders in list divided by the core count x2 as 2 threads can run on each core.
            var folderCount = dirList.Count / (coreCount * ThreadsPerCore);


            //get the list of lists of directories  split directory list.
            var splitlist = Split(dirList, folderCount);

            //add all of the new searches to a task list and start them (if we need to access these threads we can as these are stored in this list.)
            List<Thread> threads = new List<Thread>();
            foreach (List<DirectoryFolder> list in splitlist)
            {
                //start a new task with the new sub list containing the search string.
                threads.Add(new Thread(() =>
                {
                   _fileService.SearchFileForString(list, searchString, searchOptions);
                }));
            }
            //start all threads
            foreach (Thread th in threads)
            {
                th.Start();
            }

            //wait for all threads (done this by checking if thread is alive and joining each thread back on the main thread if it already has not died or ended)
            foreach (Thread t in threads)
            {
                while (t.IsAlive)
                {
                    if (!t.IsAlive)
                    {
                        t.Join();
                    }
                }
            }

            Console.Clear();
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
        }
    }
}
