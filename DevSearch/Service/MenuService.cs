﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevSearch.Interfaces;

namespace DevSearch.Service
{
    public class MenuService : IMenuService
    {
        private readonly IValidationService _validationService;
        private readonly IFileService _fileService;
        private readonly IProgramService _programService;
        private readonly ITableService _tableService;
        private readonly IDirectoryService _directoryService;
        private readonly IThreadService _threadService;

        public MenuService(IValidationService validationService, IFileService fileService
            , IProgramService programService, ITableService tableService, IDirectoryService directoryService,
            IThreadService threadService)
        {
            _validationService = validationService;
            _fileService = fileService;
            _programService = programService;
            _tableService = tableService;
            _directoryService = directoryService;
            _threadService = threadService;
        }


        public void ShowMenu()
        {
            //main menu while loop
            do
            {
                Console.WriteLine("Select the number of your choice: ");
                Console.WriteLine("1: Search for all directories of a given name within a target folder.");
                Console.WriteLine("2: Search and show all directories in a target folder.");
                Console.WriteLine("3: Search for a directory.");
                Console.WriteLine("4: Search for a directory v2");
                Console.WriteLine("5: Quit");
                Console.Write("Enter the number of your choice: ");
                var userChoice = Console.ReadLine();

                if (!int.TryParse(userChoice, out var num)) continue;

                switch (userChoice)
                {
                    case "1":
                        _directoryService.GlobalSearchForDirectory();
                        break;
                    case "2":
                        _directoryService.MapDirectory();
                        break;
                    case "3":
                        _directoryService.SearchExplicitPathForDirectory();
                        break;
                    case "4":
                        _threadService.MultiThreadedSearch();
                        break;
                    case "5":
                        Environment.Exit(0);
                        break;
                }

                Console.Clear();
            } while (true);
        }
    }
}
