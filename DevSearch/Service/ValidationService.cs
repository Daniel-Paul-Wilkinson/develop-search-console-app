﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevSearch.Interfaces;

namespace DevSearch.Service
{
    public class ValidationService : IValidationService
    {
        public bool IsValidSearch(string searchString)
        {
            if (searchString == null || searchString.Equals("") || searchString.Equals(" "))
            {
                // DPW 21.03.18 - maybe include extra parameter for custom error message...
                Console.Clear();
                Console.WriteLine("we cannot search upon spaces & the enter key please try again!");
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
