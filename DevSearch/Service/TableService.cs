﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleTableExt;
using DevSearch.Interfaces;
using DevSearch.Models;

namespace DevSearch.Service
{
    public class TableService : ITableService
    {
        public DataTable GetColumnsIfDataExists(List<DirectoryFolder> folders)
        {

            DataTable datatable = new DataTable();
            if (folders.Count > 0)
            {
                datatable.Columns.Add("Name");
                datatable.Columns.Add("Path");
                datatable.Columns.Add("Size");
                datatable.Columns.Add("Date");
            }
            else
            {
                datatable.Columns.Add("No Data");
            }
            return datatable;

        }
        public void GenerateTable(DataTable dataTable)
        {
            ConsoleTableBuilder
                .From(dataTable)
                .WithFormat(ConsoleTableBuilderFormat.Alternative)
                .ExportAndWriteLine();
        }
    }
}
