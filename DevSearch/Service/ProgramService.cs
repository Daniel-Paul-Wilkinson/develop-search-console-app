﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevSearch.Enums;
using DevSearch.Interfaces;
using DevSearch.Models;

namespace DevSearch.Service
{
    public class ProgramService : IProgramService
    {
        public void OpenInTextEditor(string path, EditIn editIn = EditIn.Notepad)
        {
            string editor = "";

            switch (editIn)
            {
                case EditIn.Notepad:
                    editor = "C:\\Program Files (x86)\\Notepad++\\Notepad++.exe";
                    break;
                case EditIn.Sublime:
                    editor = "";
                    break;
                case EditIn.Atom:
                    editor = "C:\\Users\\Daniel\\AppData\\Local\\atom\\atom.exe";
                    break;

            }

            ProcessStartInfo pi = new ProcessStartInfo(path)
            {
                Arguments = Path.GetFileName(path),
                UseShellExecute = true,
                WorkingDirectory = Path.GetDirectoryName(path),
                FileName = editor,
                Verb = "OPEN"
            };
            Process.Start(pi);
        }

        public long ReturnTotalMemoryConsumption(List<DirectoryFolder> myFolderList)
        {
            long sizeCount = 0;

            foreach (DirectoryFolder s in myFolderList)
            {
                sizeCount += Convert.ToInt32(s.Size);
            }

            return sizeCount;
        }

        public void ReadPaths(List<DirectoryFolder> list)
        {
            foreach (DirectoryFolder x in list)
            {
                Console.WriteLine(x.Path);
            }
        }

        public string FormatDirectorySize(string type, int sizes)
        {
            switch (type)
            {
                case "byte":
                    return String.Format(
                        "{0:N0} Byte", sizes);
                case "kbyte":
                    return String.Format(
                        "{0:N2} KB", ((double)sizes) / 1024);
                case "mbyte":
                    return String.Format(
                        "{0:N2} MB", ((double)sizes) / (1024 * 1024));
            }
            return "";
        }
    }
}
