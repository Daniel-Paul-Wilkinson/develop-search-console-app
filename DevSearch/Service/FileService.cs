﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevSearch.Interfaces;
using DevSearch.Models;

namespace DevSearch.Service
{
    class FileService : IFileService
    {

        private readonly IProgramService _programService;


        public FileService(IProgramService programService) {
            _programService = programService;
        }

        public void SearchFileForString(List<DirectoryFolder> list, string searchString, SearchOption SearchOptions = SearchOption.AllDirectories)
        {
            //for each dir in the list 
            foreach (DirectoryFolder f in list)
            {
                //for each file in the list
                foreach (var file in new DirectoryInfo(f.Path).EnumerateFiles("*", SearchOptions).ToList())
                {
                    //Search each file for the string... (open file?  1 = open 2 = dont)
                    SearchFile(file.FullName, searchString, 2);
                }
            }
        }

        public void SearchFile(string path, string searchString, int resultOption)
        {
            StreamReader sr;

            Console.WriteLine(path);
            try
            {
                //open the file at the file path
                using (sr = new StreamReader(path))
                {
                    //read the whole file
                    var lengthen = sr.ReadToEnd();

                    //if it contains the search query string
                    if (lengthen.Contains(searchString))
                    {

                        //open a new process to start your text editor can use anything you like:
                        //--C:\\Users\\{YOUR_NAME}\\AppData\\Local\\atom\\atom.exe
                        //--C:\\Program Files (x86)\\Notepad++\\Notepad++.exe

                        DirectoryInfo di = new DirectoryInfo(path);

                        switch (resultOption)
                        {
                            //case 1 means we will open the file
                            case 1:
                               _programService.OpenInTextEditor(path);
                                break;
                            case 2:
                                //do not open file just return the count.
                                break;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Oops cannot find this.");
            }
        }

        public string FileCount(List<DirectoryFolder> folders)
        {
            var count = folders.Sum(df => Convert.ToInt32(FileCountInSearch(new DirectoryInfo(df.Path), true)));

            return count.ToString();
        }

        public static int FileCountInSearch(DirectoryInfo di, bool searchOnlyTopDirectory)
        {
            //used to get a count of all files in a directory or all sub directories
            SearchOption searchOption = new SearchOption();
            if (searchOnlyTopDirectory) { searchOption = SearchOption.TopDirectoryOnly; }
            else { searchOption = SearchOption.AllDirectories; }
            return Directory.GetFiles(di.FullName, "*", searchOption).Length;
        }

        
    }
}
